# Un servidor Web Simple

Los objetivos conseguidos en este capitulo fue la construcción de una aplicación sencilla o sitio web con Node.js. El trabajo principal fue conocer como Node.js trabaja como servidor de aplicaciones web a través de paquetes o módulos.

La etapa inicial de la construcción del servidor web fue de forma sencilla para responder con un simple mensaje de "¡Hola Mundo!", posteriormente se expandió la capacidad del servidor web para servir archivos html y finalmente utilizamos el framework Bootstrap v4 para dar una vista agradable a nuestra aplicación.

En este capitulo Aprendimos lo siguiente:

- Instalar Node.js
- Introducción a NPM y un servidor HTTP básico.
- Servir páginas HTML
- Un Sitio Web básico
